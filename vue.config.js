module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  "outputDir": "public",
  "publicPath": "./",
  "assetsDir": "./",
  "pages": {
    "index": {
      "entry": "src/main.ts",
      "template": "assets/index.html"
    }
  },
  "pwa": {
    name: "Calculateur de salaire",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
    }
  }
}
